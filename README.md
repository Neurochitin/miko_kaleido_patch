An English patch for the visual novel [Gensou Rougoku no Kaleidoscope (幻想牢獄のカレイドスコープ)](https://vndb.org/v25583).

- Translation: 神子 (miko)
- Editing: Amarena, Neurochitin
- Graphics and technical implementation: Neurochitin

# Installation and usage instructions

First, you will need to get the game running (in Japanese) on a hacked Switch, a Switch emulator, or a hacked PS Vita. Note that the Vita patch is currently inferior to the Switch one as it does not replace the game's font. So if you have a choice, we recommend running the patched game on Switch.

Then, install the patch:

- **Switch (physical / emulator):** Go to the [Releases page](https://gitlab.com/Neurochitin/miko_kaleido_patch/-/releases) and download the “Switch patch.rom” from the latest release. Then:
    - **Emulator:** Open your emulator, right click the game and select “Open Mod Data Location” (Yuzu) or “Open Mods Directory” (Ryujinx). It should open a folder named `0100AC600EB4C000`. Inside of that folder, create another folder with any name, like `miko`. Inside of that, create yet another folder called `romfs`. Place the downloaded `patch.rom` in the `romfs` folder.
    - **Physical hacked Switch:** On your Atmosphère SD card, create the folder tree `atmosphere/contents/0100ac600eb4c000/romfs`. Place the downloaded `patch.rom` in the `romfs` folder.
- **PS Vita:** Go to the [Releases page](https://gitlab.com/Neurochitin/miko_kaleido_patch/-/releases) and download the “PS Vita patch.rom” from the latest release. Then, install the patch in `ux0/rePatch/PCSG01294/` on a homebrewed Vita with the [rePatch](https://github.com/SonicMastr/rePatch-reLoaded) plugin installed. **(Note that the Vita patch is inferior to the Switch one, because so far, we were unable to patch the game's font on the Vita. Instead, the default font is used, which is a bit ugly and is also missing some characters. For the best experience, )

Then, launch the game. It should now be translated to English, including most of the GUI. If everything is still in Japanese, you must have done something wrong.

# Issues / bug reports

Feel free to open an issue here on GitLab to report any problem. You can also contact us on Discord (`neurochitin` / `blancdecerise`) — send us a friend request or join the [07th Mod server](https://discord.gg/pf5VhF9); we're in the “Gerokasu dev” thread in `#side-projects`.

# Compilation

Follow these instructions if you want to build the patch yourself from source.

**Note for Windows users:** This repository uses symbolic links. Either follow [these instructions](https://stackoverflow.com/a/59761201) before cloning this repository, or move `font/varela/varela.fnt` to `rom_source/newrodin.fnt` (replacing the existing file) after cloning. Also, it is unclear if it is possible to compile EnterExtractor for Windows natively, although there should be no problems in WSL.

You will need:

- Ruby
- ImageMagick
- [Kaleido](https://gitlab.com/Neurochitin/kaleido), branch `miko_kaleido`, cloned locally
- [EnterExtractor](https://github.com/07th-mod/enter_extractor)
- The `data.rom` of 幻想牢獄のカレイドスコープ extracted to a folder tree, for example using EnterExtractor's `blabla.py`, or using [sdu](https://github.com/DCNick3/shin#what-else-is-in-the-box) (`sdu rom extract path/to/data.rom path/to/output_folder`)

Set the constants `KALEIDO_PATH` and `EXTRACT_PATH` in `compile.rb` to the respective folder paths on your system, and `EE_PATH` to the compiled EnterExtractor executable. Then, run

```
ruby compile.rb path/to/patch.rom
```

which will compile the scripts to a `patch.rom` at the given path.

(These instructions create a `patch.rom` for Switch. The difference between the Switch and Vita patches is that the Vita version of the engine uses a different font format that has not yet been fully reverse-engineered. So you can make a patch for the Vita by deleting the `rom_source/newrodin.fnt` file before running `compile.rb`.)
