require_relative 'instructions.rb'
require_relative 'registers.rb'

require_relative 'tables/masks.rb'
require_relative 'tables/backgrounds.rb'
require_relative 'tables/bustups.rb'
require_relative 'tables/bgm.rb'
require_relative 'tables/se.rb'
require_relative 'tables/movies.rb'
require_relative 'tables/voices.rb'
require_relative 'tables/table8.rb'
require_relative 'tables/table9.rb'

require_relative 'script/functions.rb'
require_relative 'script/prelude.rb'

require_relative 'script/s00_prologue.rb'
require_relative 'script/s02_intA01.rb'
require_relative 'script/s02_intA02.rb'
require_relative 'script/s03_gameA.rb'
require_relative 'script/s04_event.rb'
require_relative 'script/s05_gameB_A.rb'
require_relative 'script/s06_intB.rb'
require_relative 'script/s07_gameB_B.rb'
require_relative 'script/s08_intZ.rb'
require_relative 'script/s09_epilogue.rb'

require_relative 'script/debug_menu.rb'
require_relative 'script/debug_test.rb'

def raw_apply(snr)
  write_masks(snr)
  write_backgrounds(snr)
  write_bustups(snr)
  write_bgm(snr)
  write_se(snr)
  write_movies(snr)
  write_voices(snr)
  write_table8(snr)
  write_table9(snr)

  s = KalScript.new(snr.current_offset + 8)
  s.layouter = snr.extra[:layouter]
  s.ins JUMP, :addr_0xb9f7a

  write_functions(s)
  write_prelude(s)

  write_s00_prologue(s)
  write_s02_intA01(s)
  write_s02_intA02(s)
  write_s03_gameA(s)
  write_s04_event(s)
  write_s05_gameB_A(s)
  write_s06_intB(s)
  write_s07_gameB_B(s)
  write_s08_intZ(s)
  write_s09_epilogue(s)

  s.label :addr_0xb9f68
  s.ins MOV, byte(0), ushort(69), 1
  s.ins SSET, byte(0), byte(1)
  s.ins TROPHY, byte(26)
  s.ins EXIT
  s.ins EXIT
  s.ins EXIT

  entry_point = s.label :entry_point
  s.ins JUMP, :game_00_prologue
  s.label :addr_0xb9f7a
  s.ins CALL, :f_reset, []

  write_debug_menu(s)
  write_debug_test(s)

  snr.write_script(s.data, $entry_point_override.nil? ? entry_point : (s.labels[$entry_point_override]), s.dialogue_line_count)
end
