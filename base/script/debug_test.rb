require_relative '../instructions.rb'
require_relative '../registers.rb'

def write_debug_test(s)
  s.label :debug_test
  s.ins CALL, :f_reset, []
  s.ins JUMP, :addr_0x8b36
  s.ins MSGSET, uint(6243), byte(1), line(s, 6243)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins MOV, byte(0), ushort(R_temp), 3
  s.ins LAYERLOAD, 0, byte(2), 0, byte(1), 45
  s.ins LAYERCTRL, 0, 12, byte(1), 900
  s.ins LAYERCTRL, 0, 13, byte(1), 900
  s.ins LAYERLOAD, 1, byte(9), 0, 2, Register.new(R_temp)
  s.ins 0xe0, 1, 0, 1
  s.ins PLANESELECT, byte(1)
  s.ins MASKLOAD, 79, 0, 0
  s.ins LAYERLOAD, 0, byte(2), 0, byte(1), 67
  s.ins PLANESELECT, byte(0)
  s.ins MSGSET, uint(6244), byte(1), line(s, 6244)
  s.ins MSGWAIT, byte(127)
  s.ins PLANESELECT, byte(1)
  s.ins LAYERCTRL, -4, 18, byte(3), -249, 60
  s.ins PLANESELECT, byte(0)
  s.ins MSGSET, uint(6245), byte(1), line(s, 6245)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(6246), byte(1), line(s, 6246)
  s.ins MSGWAIT, byte(127)
  s.ins BGMPLAY, 0, 0, 0, 1000
  s.ins PAGEBACK
  s.ins LAYERLOAD, 0, byte(2), 0, byte(1), 38
  s.ins LAYERCTRL, 0, 2, byte(1), 3333
  s.ins WIPE, 0, 2, 60, byte(0)
  s.ins WIPEWAIT
  s.ins MSGSET, uint(16783463), byte(1), line(s, 16783463)
  s.ins MSGSET, uint(16783464), byte(2), line(s, 16783464)
  s.ins MSGSET, uint(16783465), byte(3), line(s, 16783465)
  s.ins MSGSET, uint(16783466), byte(0), line(s, 16783466)
  s.ins SELECT, ushort(0), ushort(0), ushort(R_temp), 4095, 'NCSELECT', "選択肢\x00選択肢１\x00選択肢２\x00選択肢３\x00"

  6.times { s.ins EXIT }

  s.ins LAYERLOAD, 1, byte(3), 0, byte(1), 16
  s.ins LAYERCTRL, 1, 2, byte(1), 1538
  s.ins LAYERCTRL, 1, 18, byte(1), -28
  s.ins LAYERCTRL, 1, 0, byte(1), 200
  s.ins LAYERLOAD, 2, byte(3), 0, byte(1), 32
  s.ins LAYERCTRL, 2, 2, byte(1), 1000
  s.ins LAYERCTRL, 2, 18, byte(1), -28
  s.ins LAYERCTRL, 2, 0, byte(1), -199
  s.ins LAYERLOAD, 3, byte(3), 0, byte(1), 53
  s.ins LAYERCTRL, 3, 2, byte(1), 666
  s.ins LAYERCTRL, 3, 18, byte(1), -28
  s.ins LAYERCTRL, 3, 0, byte(1), -199
  s.ins LAYERLOAD, 4, byte(3), 0, byte(1), 4
  s.ins LAYERCTRL, 4, 2, byte(1), 500
  s.ins LAYERCTRL, 4, 18, byte(1), 28
  s.ins LAYERCTRL, 4, 0, byte(1), 400

  15.times { s.ins EXIT }
end
