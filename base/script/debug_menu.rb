require_relative '../instructions.rb'
require_relative '../registers.rb'

def write_debug_menu(s)
  s.label :debug_menu
  s.ins SELECT, ushort(0), ushort(0), ushort(R_temp), 4095, 'NCSELECT', "Script test\x00Test\x00Start normally\x00"
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(3), :debug_script_test, :debug_test, :addr_0xb9fcb
  s.label :addr_0xb9fcb
  s.ins MOV, byte(0), ushort(0), 0
  s.ins JUMP, :entry_point
  s.label :debug_script_test
  s.ins SELECT, ushort(0), ushort(0), ushort(R_temp), 4095, 'NCSELECT', "Back\x0000_prologue\x0002_intA01\x0002_intA02\x0003_gameA\x0004_event\x0005_gameB前半\x00→\x00"
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(8), :debug_menu, :debug_script_00_prologue, :debug_script_02_intA01, :debug_script_02_intA02, :debug_script_03_gameA, :debug_script_04_event, :debug_script_05_gameB_1st_half, :debug_script_test_page2
  s.label :debug_script_test_page2
  s.ins SELECT, ushort(0), ushort(0), ushort(R_temp), 4095, 'NCSELECT', "←\x0006_intB01/02\x0007_gameB後半\x0008_intZ01/02\x0009_epilogue\x00"
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(5), :debug_script_test, :debug_script_06_intB01_and_02, :debug_script_07_gameB_2nd_half, :debug_script_08_intZ01_and_02, :debug_script_09_epilogue
  s.label :debug_script_00_prologue
  s.ins GOSUB, :game_00_prologue
  s.ins JUMP, :debug_script_test
  s.label :debug_script_09_epilogue
  s.ins GOSUB, :game_09_epilogue
  s.ins JUMP, :debug_script_test_page2
  s.label :debug_script_02_intA01
  s.ins SELECT, ushort(0), ushort(0), ushort(R_temp), 4095, 'NCSELECT', "Back\x0002_intA01_01_風華、初めての死刑囚\x0002_intA01_02_火凛、初めての死刑囚\x0002_intA01_03_水無、初めての死刑囚\x0002_intA01_04_土麗美、初めての死刑囚\x0002_intA01_05_風華、２回目の死刑囚\x0002_intA01_06_火凛、２回目の死刑囚\x00→\x00"
  s.ins JUMP_COND, byte(0), Register.new(R_temp), 0, :debug_menu
  s.ins JUMP_COND, byte(0), Register.new(R_temp), 7, :debug_script_02_intA01_page2
  s.ins MOV, byte(3), ushort(R_temp), 1
  s.ins GOSUB_TABLE, Register.new(R_temp), ushort(6), :game_02_intA01_01_風華、初めての死刑囚, :game_02_intA01_02_火凛、初めての死刑囚, :game_02_intA01_03_水無、初めての死刑囚, :game_02_intA01_04_土麗美、初めての死刑囚, :game_02_intA01_05_風華、２回目の死刑囚, :game_02_intA01_06_火凛、２回目の死刑囚
  s.ins JUMP, :debug_script_02_intA01
  s.label :debug_script_02_intA01_page2
  s.ins SELECT, ushort(0), ushort(0), ushort(R_temp), 4095, 'NCSELECT', "←\x0002_intA01_07_水無、２回目の死刑囚\x0002_intA01_08_土麗美、２回目の死刑囚\x00"
  s.ins JUMP_COND, byte(0), Register.new(R_temp), 0, :debug_script_02_intA01
  s.ins MOV, byte(3), ushort(R_temp), 1
  s.ins GOSUB_TABLE, Register.new(R_temp), ushort(2), :game_02_intA01_07_水無、２回目の死刑囚, :game_02_intA01_08_土麗美、２回目の死刑囚
  s.ins JUMP, :debug_script_02_intA01_page2
  s.label :debug_script_02_intA02
  s.ins SELECT, ushort(0), ushort(0), ushort(R_temp), 4095, 'NCSELECT', "Back\x0002_intA02_01_初めての、ゲーム開始\x0002_intA02_02_初めての、ゲーム開始２\x0002_intA02_03_初めての、ゲーム開始３\x0002_intA02_04_初めての、ゲーム開始４\x0002_intA02_05_初めての、ゲーム開始５\x0002_intA02_06_初めての、ゲーム開始６\x00→\x00"
  s.ins JUMP_COND, byte(0), Register.new(R_temp), 0, :debug_menu
  s.ins JUMP_COND, byte(0), Register.new(R_temp), 7, :addr_0xba3a1
  s.ins MOV, byte(3), ushort(R_temp), 1
  s.ins GOSUB_TABLE, Register.new(R_temp), ushort(6), :game_02_intA02_01_初めての、ゲーム開始, :game_02_intA02_02_初めての、ゲーム開始２, :game_02_intA02_03_初めての、ゲーム開始３, :game_02_intA02_04_初めての、ゲーム開始４, :game_02_intA02_05_初めての、ゲーム開始５, :game_02_intA02_06_初めての、ゲーム開始６
  s.ins JUMP, :debug_script_02_intA02
  s.label :addr_0xba3a1
  s.ins SELECT, ushort(0), ushort(0), ushort(R_temp), 4095, 'NCSELECT', "←\x0002_intA02_07_初めての、ゲーム開始７\x0002_intA02_08_初めての、ゲーム開始８\x0002_intA02_09_初めての、ゲーム開始９\x0002_intA02_10_初めての、ゲーム開始１０\x0002_intA02_11_初めての、ゲーム開始１１\x0002_intA02_12_初めての、ゲーム開始１２\x00"
  s.ins JUMP_COND, byte(0), Register.new(R_temp), 0, :debug_script_02_intA02
  s.ins MOV, byte(3), ushort(R_temp), 1
  s.ins GOSUB_TABLE, Register.new(R_temp), ushort(6), :game_02_intA02_07_初めての、ゲーム開始７, :game_02_intA02_08_初めての、ゲーム開始８, :game_02_intA02_09_初めての、ゲーム開始９, :game_02_intA02_10_初めての、ゲーム開始１０, :game_02_intA02_11_初めての、ゲーム開始１１, :game_02_intA02_12_初めての、ゲーム開始１２
  s.ins JUMP, :addr_0xba3a1
  s.label :debug_script_03_gameA
  s.ins SELECT, ushort(0), ushort(0), ushort(R_temp), 4095, 'NCSELECT', "Back\x0003_gameA_01（死：風、ピ：火）風華属性\x0003_gameA_02（死：風、ピ：水）風華属性\x0003_gameA_03（死：風、ピ：土）土麗美属性\x0003_gameA_04（死：火、ピ：風）風華属性\x0003_gameA_05（死：火、ピ：水）火凛属性\x0003_gameA_06（死：火、ピ：土）火凛属性\x00→\x00"
  s.ins JUMP_COND, byte(0), Register.new(R_temp), 0, :debug_menu
  s.ins JUMP_COND, byte(0), Register.new(R_temp), 7, :addr_0xba604
  s.ins MOV, byte(3), ushort(R_temp), 1
  s.ins GOSUB_TABLE, Register.new(R_temp), ushort(6), :game_03_gameA_01（死：風、ピ：火）風華属性, :game_03_gameA_02（死：風、ピ：水）風華属性, :game_03_gameA_03（死：風、ピ：土）土麗美属性, :game_03_gameA_04（死：火、ピ：風）風華属性, :game_03_gameA_05（死：火、ピ：水）火凛属性, :game_03_gameA_06（死：火、ピ：土）火凛属性
  s.ins JUMP, :debug_script_03_gameA
  s.label :addr_0xba604
  s.ins SELECT, ushort(0), ushort(0), ushort(R_temp), 4095, 'NCSELECT', "←\x0003_gameA_07（死：水、ピ：風）水無属性\x0003_gameA_08（死：水、ピ：火）火凛属性\x0003_gameA_09（死：水、ピ：土）水無属性\x0003_gameA_10（死：土、ピ：風）土麗美属性\x0003_gameA_11（死：土、ピ：火）土麗美属性\x0003_gameA_12（死：土、ピ：水）水無属性\x00"
  s.ins JUMP_COND, byte(0), Register.new(R_temp), 0, :debug_script_03_gameA
  s.ins MOV, byte(3), ushort(R_temp), 1
  s.ins GOSUB_TABLE, Register.new(R_temp), ushort(6), :game_03_gameA_07（死：水、ピ：風）水無属性, :game_03_gameA_08（死：水、ピ：火）火凛属性, :game_03_gameA_09（死：水、ピ：土）水無属性, :game_03_gameA_10（死：土、ピ：風）土麗美属性, :game_03_gameA_11（死：土、ピ：火）土麗美属性, :game_03_gameA_12（死：土、ピ：水）水無属性
  s.ins JUMP, :addr_0xba604
  s.label :debug_script_04_event
  s.ins SELECT, ushort(0), ushort(0), ushort(R_temp), 4095, 'NCSELECT', "Back\x0004_event01_風華イベント１\x0004_event02_風華イベント２\x0004_event03_風華イベント３\x0004_event04_火凛イベント１\x0004_event05_火凛イベント２\x0004_event06_火凛イベント３\x00→\x00"
  s.ins JUMP_COND, byte(0), Register.new(R_temp), 0, :debug_menu
  s.ins JUMP_COND, byte(0), Register.new(R_temp), 7, :addr_0xba950
  s.ins MOV, byte(3), ushort(R_temp), 1
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(6), :debug_script_04_fuuka_event, :debug_script_04_fuuka_event, :debug_script_04_fuuka_event, :debug_script_04_karin_event, :debug_script_04_karin_event, :debug_script_04_karin_event
  s.ins JUMP, :debug_script_04_event
  s.label :debug_script_04_fuuka_event
  s.ins SELECT, ushort(0), ushort(0), ushort(R_route_index), 4095, 'NCSELECT', "Back\x00１番から\x00２番から\x00４番から\x00"
  s.ins JUMP_COND, byte(0), Register.new(R_route_index), 0, :debug_script_04_event
  s.ins MOV, byte(3), ushort(R_route_index), 1
  s.ins CALC, ushort(R_temp), byte(0), Register.new(R_temp), byte(0), 3, byte(3), byte(0), Register.new(R_route_index), byte(1), byte(255)
  s.ins TABLEGET, ushort(R_route_index), Register.new(R_route_index), ushort(3), 1, byte(0), byte(0), byte(0), 2, byte(0), byte(0), byte(0), 4, byte(0), byte(0), byte(0)
  s.ins GOSUB_TABLE, Register.new(R_temp), ushort(9), :game_04_event_fuuka_1, :game_04_event_fuuka_2, :game_04_event_fuuka_3, :game_04_event_fuuka_4, :game_04_event_fuuka_5, :game_04_event_fuuka_6, :game_04_event_fuuka_7, :game_04_event_fuuka_8, :game_04_event_fuuka_9
  s.ins JUMP, :debug_script_04_event
  s.label :debug_script_04_karin_event
  s.ins SELECT, ushort(0), ushort(0), ushort(R_route_index), 4095, 'NCSELECT', "Back\x00５番から\x00６番から\x00８番から\x00"
  s.ins JUMP_COND, byte(0), Register.new(R_route_index), 0, :debug_script_04_event
  s.ins MOV, byte(3), ushort(R_route_index), 1
  s.ins CALC, ushort(R_temp), byte(0), Register.new(R_temp), byte(0), 3, byte(2), byte(0), 3, byte(3), byte(0), Register.new(R_route_index), byte(1), byte(255)
  s.ins TABLEGET, ushort(R_route_index), Register.new(R_route_index), ushort(3), 5, byte(0), byte(0), byte(0), 6, byte(0), byte(0), byte(0), 8, byte(0), byte(0), byte(0)
  s.ins GOSUB_TABLE, Register.new(R_temp), ushort(9), :game_04_event_karin_1, :game_04_event_karin_2, :game_04_event_karin_3, :game_04_event_karin_4, :game_04_event_karin_5, :game_04_event_karin_6, :game_04_event_karin_7, :game_04_event_karin_8, :game_04_event_karin_9
  s.ins JUMP, :debug_script_04_event
  s.label :addr_0xba950
  s.ins SELECT, ushort(0), ushort(0), ushort(R_temp), 4095, 'NCSELECT', "←\x0004_event07_水無イベント１\x0004_event08_水無イベント２\x0004_event09_水無イベント３\x0004_event10_土麗美イベント１\x0004_event11_土麗美イベント２\x0004_event12_土麗美イベント３\x00"
  s.ins JUMP_COND, byte(0), Register.new(R_temp), 0, :debug_script_04_event
  s.ins MOV, byte(3), ushort(R_temp), 1
  s.ins GOSUB_TABLE, Register.new(R_temp), ushort(6), :debug_script_04_mina_event, :debug_script_04_mina_event, :debug_script_04_mina_event, :debug_script_04_doremi_event, :debug_script_04_doremi_event, :debug_script_04_doremi_event
  s.ins JUMP, :addr_0xba950
  s.label :debug_script_04_mina_event
  s.ins SELECT, ushort(0), ushort(0), ushort(R_route_index), 4095, 'NCSELECT', "Back\x00７番から\x00９番から\x00１２番から\x00"
  s.ins JUMP_COND, byte(0), Register.new(R_route_index), 0, :addr_0xba950
  s.ins MOV, byte(3), ushort(R_route_index), 1
  s.ins CALC, ushort(R_temp), byte(0), Register.new(R_temp), byte(0), 3, byte(3), byte(0), Register.new(R_route_index), byte(1), byte(255)
  s.ins TABLEGET, ushort(R_route_index), Register.new(R_route_index), ushort(3), 7, byte(0), byte(0), byte(0), 9, byte(0), byte(0), byte(0), 12, byte(0), byte(0), byte(0)
  s.ins GOSUB_TABLE, Register.new(R_temp), ushort(9), :game_04_event_mina_1, :game_04_event_mina_2, :game_04_event_mina_3, :game_04_event_mina_4, :game_04_event_mina_5, :game_04_event_mina_6, :game_04_event_mina_7, :game_04_event_mina_8, :game_04_event_mina_9
  s.ins JUMP, :addr_0xba950
  s.label :debug_script_04_doremi_event
  s.ins SELECT, ushort(0), ushort(0), ushort(R_route_index), 4095, 'NCSELECT', "Back\x00３番から\x00１０番から\x00１１番から\x00"
  s.ins JUMP_COND, byte(0), Register.new(R_route_index), 0, :addr_0xba950
  s.ins MOV, byte(3), ushort(R_route_index), 1
  s.ins CALC, ushort(R_temp), byte(0), Register.new(R_temp), byte(0), 3, byte(2), byte(0), 3, byte(3), byte(0), Register.new(R_route_index), byte(1), byte(255)
  s.ins TABLEGET, ushort(R_route_index), Register.new(R_route_index), ushort(3), 3, byte(0), byte(0), byte(0), 10, byte(0), byte(0), byte(0), 11, byte(0), byte(0), byte(0)
  s.ins GOSUB_TABLE, Register.new(R_temp), ushort(9), :game_04_event_doremi_1, :game_04_event_doremi_2, :game_04_event_doremi_3, :game_04_event_doremi_4, :game_04_event_doremi_5, :game_04_event_doremi_6, :game_04_event_doremi_7, :game_04_event_doremi_8, :game_04_event_doremi_9
  s.ins JUMP, :addr_0xba950
  s.label :debug_script_05_gameB_1st_half
  s.ins SELECT, ushort(0), ushort(0), ushort(R_temp), 4095, 'NCSELECT', "Back\x0005_gameB前半01\x0005_gameB前半02\x0005_gameB前半03\x0005_gameB前半04\x0005_gameB前半05\x0005_gameB前半06\x00→\x00"
  s.ins JUMP_COND, byte(0), Register.new(R_temp), 0, :debug_menu
  s.ins JUMP_COND, byte(0), Register.new(R_temp), 7, :addr_0xbac1a
  s.ins MOV, byte(3), ushort(R_temp), 1
  s.ins GOSUB_TABLE, Register.new(R_temp), ushort(6), :game_05_gameB前半01, :game_05_gameB前半02, :game_05_gameB前半03, :game_05_gameB前半04, :game_05_gameB前半05, :game_05_gameB前半06
  s.ins JUMP, :debug_script_05_gameB_1st_half
  s.label :addr_0xbac1a
  s.ins SELECT, ushort(0), ushort(0), ushort(R_temp), 4095, 'NCSELECT', "←\x0005_gameB前半07\x0005_gameB前半08\x0005_gameB前半09\x0005_gameB前半10\x0005_gameB前半11\x0005_gameB前半12\x00"
  s.ins JUMP_COND, byte(0), Register.new(R_temp), 0, :debug_script_05_gameB_1st_half
  s.ins MOV, byte(3), ushort(R_temp), 1
  s.ins GOSUB_TABLE, Register.new(R_temp), ushort(6), :game_05_gameB前半07, :game_05_gameB前半08, :game_05_gameB前半09, :game_05_gameB前半10, :game_05_gameB前半11, :game_05_gameB前半12
  s.ins JUMP, :addr_0xbac1a
  s.label :debug_script_06_intB01_and_02
  s.ins SELECT, ushort(0), ushort(0), ushort(R_temp), 4095, 'NCSELECT', "Back\x0006_intB01_01_初めての風華イベント時\x0006_intB01_02_初めての火凛イベント時\x0006_intB01_03_初めての水無イベント時\x0006_intB01_04_初めての土麗美イベント時\x0006_intB02_01_それ以外時のイベント（１回目）\x0006_intB02_02_それ以外時のイベント（２回目）\x00→\x00"
  s.ins JUMP_COND, byte(0), Register.new(R_temp), 0, :debug_menu
  s.ins JUMP_COND, byte(0), Register.new(R_temp), 7, :addr_0xbadfd
  s.ins MOV, byte(3), ushort(R_temp), 1
  s.ins GOSUB_TABLE, Register.new(R_temp), ushort(6), :game_06_intB01_01_初めての風華イベント時, :game_06_intB01_02_初めての火凛イベント時, :game_06_intB01_03_初めての水無イベント時, :game_06_intB01_04_初めての土麗美イベント時, :game_06_intB02_01_それ以外時のイベント（１回目）, :game_06_intB02_02_それ以外時のイベント（２回目）
  s.ins JUMP, :debug_script_06_intB01_and_02
  s.label :addr_0xbadfd
  s.ins SELECT, ushort(0), ushort(0), ushort(R_temp), 4095, 'NCSELECT', "←\x0006_intB02_03_それ以外時のイベント（３回目）\x0006_intB02_04_それ以外時のイベント（４回目）\x0006_intB02_05_それ以外時のイベント（５回目）\x0006_intB02_06_それ以外時のイベント（６回目）\x0006_intB02_07_それ以外時のイベント（７回目）\x0006_intB02_08_それ以外時のイベント（８回目）\x00"
  s.ins JUMP_COND, byte(0), Register.new(R_temp), 0, :debug_script_06_intB01_and_02
  s.ins MOV, byte(3), ushort(R_temp), 1
  s.ins GOSUB_TABLE, Register.new(R_temp), ushort(6), :game_06_intB02_03_それ以外時のイベント（３回目）, :game_06_intB02_04_それ以外時のイベント（４回目）, :game_06_intB02_05_それ以外時のイベント（５回目）, :game_06_intB02_06_それ以外時のイベント（６回目）, :game_06_intB02_07_それ以外時のイベント（７回目）, :game_06_intB02_08_それ以外時のイベント（８回目）
  s.ins JUMP, :addr_0xbadfd
  s.label :debug_script_07_gameB_2nd_half
  s.ins SELECT, ushort(0), ushort(0), ushort(R_temp), 4095, 'NCSELECT', "Back\x0007_gameB後半01\x0007_gameB後半02\x0007_gameB後半03\x0007_gameB後半04\x0007_gameB後半05\x0007_gameB後半06\x00→\x00"
  s.ins JUMP_COND, byte(0), Register.new(R_temp), 0, :debug_menu
  s.ins JUMP_COND, byte(0), Register.new(R_temp), 7, :addr_0xbaffe
  s.ins MOV, byte(3), ushort(R_temp), 1
  s.ins GOSUB_TABLE, Register.new(R_temp), ushort(6), :game_07_gameB後半01, :game_07_gameB後半02, :game_07_gameB後半03, :game_07_gameB後半04, :game_07_gameB後半05, :game_07_gameB後半06
  s.ins JUMP, :debug_script_07_gameB_2nd_half
  s.label :addr_0xbaffe
  s.ins SELECT, ushort(0), ushort(0), ushort(R_temp), 4095, 'NCSELECT', "←\x0007_gameB後半07\x0007_gameB後半08\x0007_gameB後半09\x0007_gameB後半10\x0007_gameB後半11\x0007_gameB後半12\x00"
  s.ins JUMP_COND, byte(0), Register.new(R_temp), 0, :debug_script_07_gameB_2nd_half
  s.ins MOV, byte(3), ushort(R_temp), 1
  s.ins GOSUB_TABLE, Register.new(R_temp), ushort(6), :game_07_gameB後半07, :game_07_gameB後半08, :game_07_gameB後半09, :game_07_gameB後半10, :game_07_gameB後半11, :game_07_gameB後半12
  s.ins JUMP, :addr_0xbaffe
  s.label :debug_script_08_intZ01_and_02
  s.ins SELECT, ushort(0), ushort(0), ushort(R_temp), 4095, 'NCSELECT', "Back\x0008_intZ01_01_初めての風華イベント時\x0008_intZ01_02_初めての火凛イベント時\x0008_intZ01_03_初めての水無イベント時\x0008_intZ01_04_初めての土麗美イベント時\x0008_intZ02_01_その他（１度目）\x0008_intZ02_02_その他（２度目）\x00→\x00"
  s.ins JUMP_COND, byte(0), Register.new(R_temp), 0, :debug_menu
  s.ins JUMP_COND, byte(0), Register.new(R_temp), 7, :addr_0xbb1c5
  s.ins MOV, byte(3), ushort(R_temp), 1
  s.ins GOSUB_TABLE, Register.new(R_temp), ushort(6), :game_08_intZ01_01_初めての風華イベント時, :game_08_intZ01_02_初めての火凛イベント時, :game_08_intZ01_03_初めての水無イベント時, :game_08_intZ01_04_初めての土麗美イベント時, :game_08_intZ02_01_その他（１度目）, :game_08_intZ02_02_その他（２度目）
  s.ins JUMP, :debug_script_08_intZ01_and_02
  s.label :addr_0xbb1c5
  s.ins SELECT, ushort(0), ushort(0), ushort(R_temp), 4095, 'NCSELECT', "←\x0008_intZ02_03_その他（３度目）\x0008_intZ02_04_その他（４度目）\x0008_intZ02_05_その他（５度目）\x0008_intZ02_06_その他（６度目）\x0008_intZ02_07_その他（７度目）\x0008_intZ02_08_その他（８度目）\x00"
  s.ins JUMP_COND, byte(0), Register.new(R_temp), 0, :debug_script_08_intZ01_and_02
  s.ins MOV, byte(3), ushort(R_temp), 1
  s.ins GOSUB_TABLE, Register.new(R_temp), ushort(6), :game_08_intZ02_03_その他（３度目）, :game_08_intZ02_04_その他（４度目）, :game_08_intZ02_05_その他（５度目）, :game_08_intZ02_06_その他（６度目）, :game_08_intZ02_07_その他（７度目）, :game_08_intZ02_08_その他（８度目）
  s.ins JUMP, :addr_0xbb1c5
end
