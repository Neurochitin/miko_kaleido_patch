def write_bgm(snr)
  snr.bgm 'bgm01', %(WaterRipples), 65535
  snr.bgm 'bgm02', %(MindNoiz), 65535
  snr.bgm 'bgm03', %(Narawashi), 65535
  snr.bgm 'bgm04', %(HYS), 65535
  snr.bgm 'bgm05', %(Inishie), 65535
  snr.bgm 'bgm06', %(peace), 65535
  snr.bgm 'bgm07', %(eye to eye), 65535
  snr.bgm 'bgm08', %(BraveryEyes), 65535
  snr.bgm 'bgm09', %(Full heart), 65535
  snr.bgm 'bgm10', %(ConvictionFES.), 65535
  snr.bgm 'bgm11', %(DoRememberMe.), 65535
  snr.bgm 'bgm12', %(ぼくらの果てに), 65535
  snr.bgm 'bgm13', %(LittleAnswer), 65535
  snr.bgm 'op1', %(Beautiful world), 65535
  snr.bgm 'op2', %(I'm the great Pretender), 65535
  snr.write_bgms
end
